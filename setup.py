
from setuptools import setup, find_packages

with open('VERSION', 'r') as ifd:
	VERSION = ifd.read().strip()

with open('README.md', 'r') as ifd:
	README = ifd.read()

setup(
	name='pex',
	version=VERSION,
	author='Ivan Simonini',
	author_email='ivan.simonini@roundhousecode.com',
	description='Published example with duplicated resources',
	long_description=README,
	long_description_content_type='text/markdown',
	url='https://gitlab.com/drachlyznardh/pex',
	package_dir={'':'src'},
	packages=find_packages(where='src'),
	package_data={'': ['VERSION', 'LICENSE', 'DESCRIPTION', '*.lic', '*.key',
			'platforms/*/*/*.so', 'platforms/*/*/*.dll']},
	data_files=[('', ['VERSION', 'LICENSE', 'README.md'])],
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: MIT License",
		"Operating System :: OS Independent",
	],
	entry_points={'console_scripts':['pex=pex.pex:main']},
)

