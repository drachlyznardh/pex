
all: test

mkenv:
	@python3 -m venv /tmp/pex

PLATFORMS=--platform linux.x86_64 --platform windows.x86_64 --platform windows.x86

build: veryclean
	@pyarmor obfuscate $(PLATFORMS) --output src/pex/ plain/pex/__init__.py
	@cp -r res/pex/* src/pex/
	@python3 setup.py sdist

install:
	@python3 setup.py install

userinstall:
	@pip install --user dist/*.tar.gz

remove:
	@pip uninstall -y pex

clean:
	@python3 setup.py clean

veryclean: clean
	@rm -rf build/ dist/ src/
	@if test -e src/ ; then find src/ -name '*.egg-info' -delete; fi

test:
	@python3 -c 'import pex'
	@python3 -c 'import pex; pex.print_version()' | diff - VERSION
	@python3 -m pex
	@python3 -m pex --version | diff - VERSION
	@pex
	@pex --version | diff - VERSION
	@pex --license | diff - LICENSE
	@pex --description | diff -wB - README.md

dist:
	@python3 setup.py sdist bdist_wheel

upload:
	@python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*.tar.gz

remoteinstall:
	@pip install -i https://test.pypi.org/simple pex

remoteuserinstall:
	@pip install --user -i https://test.pypi.org/simple pex

.PHONY: dist

