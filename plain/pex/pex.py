
def print_version():
	import pkg_resources
	with open(pkg_resources.resource_filename(__name__, 'VERSION'), 'r') as ifd:
		print(ifd.read().strip())

def print_license():
	import pkg_resources
	with open(pkg_resources.resource_filename(__name__, 'LICENSE'), 'r') as ifd:
		print(ifd.read().strip())

def print_description():
	import pkg_resources
	with open(pkg_resources.resource_filename(__name__, 'DESCRIPTION'), 'r') as ifd:
		print(ifd.read().strip())

def parse(inargs):
	import optparse, sys
	p = optparse.OptionParser()
	p.add_option('-v', '--version', action='store_true', default=False, help='Print version and exit')
	p.add_option('-l', '--license', action='store_true', default=False, help='Print license and exit')
	p.add_option('-d', '--description', action='store_true', default=False, help='Print description and exit')

	return p.parse_args(inargs or sys.argv[1:])

def main(inargs=None):

	o, args = parse(inargs)

	if o.version:
		print_version()
		return 0

	if o.license:
		print_license()
		return 0

	if o.description:
		print_description()
		return 0

	return 0

