
# Pex

This is a Published Example, done following
https://packaging.python.org/tutorials/packaging-projects/. I want to see if I
can include both VERSION and REAMDE as resources as well, though. Turns out I
can.

Apparently, I can also obfuscate this with PyArmor (using the default key, as I
don't intend spending money on it) and make it available for three platforms I
choose.

